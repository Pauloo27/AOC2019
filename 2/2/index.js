const fs = require("fs");

function resolve(program, offset) {
  let opcode = program[offset];
  let input1Index = program[offset + 1];
  let input2Index = program[offset + 2];
  let outputIndex = program[offset + 3];

  if (opcode == 99) return program;

  if (opcode == 1) {
    program[outputIndex] = program[input1Index] + program[input2Index];
  }

  if (opcode == 2) {
    program[outputIndex] = program[input1Index] * program[input2Index];
  }

  return resolve(program, offset + 4);
}

const input = fs.readFileSync("../input.txt").toString();
const instructions = input.split(",").map(element => Number.parseInt(element));

function test(instructions, noum, verb) {
  instructions[1] = noum;
  instructions[2] = verb;

  return resolve(instructions, 0)[0];
}

const wantedOutput = 19690720;
for (let noum = 0; noum <= 99; noum++) {
  for (let verb = 0; verb <= 99; verb++) {
    console.log(`Testing with noum ${noum} & verb ${verb}...`);
    if (test(instructions.slice(), noum, verb) == wantedOutput) {
      console.log("Founded.");
      console.log(`Final result = ${100 * noum + verb}`);
      process.exit(0);
    }
  }
}
