const fs = require("fs");

function resolve(program, offset) {
  let opcode = program[offset];
  let input1Index = program[offset + 1];
  let input2Index = program[offset + 2];
  let outputIndex = program[offset + 3];

  if (opcode == 99) return program;

  if (opcode == 1) {
    program[outputIndex] = program[input1Index] + program[input2Index];
  }

  if (opcode == 2) {
    program[outputIndex] = program[input1Index] * program[input2Index];
  }

  return resolve(program, offset + 4);
}

const input = fs.readFileSync("../input.txt").toString();
const instructions = input.split(",").map(element => Number.parseInt(element));

//  replace position 1 with the value 12 and replace position 2 with the value 2
instructions[1] = 12;
instructions[2] = 2;

console.log(resolve(instructions, 0)[0]);
