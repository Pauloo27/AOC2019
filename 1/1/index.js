const fs = require("fs");

function getFuel(mass) {
  return Math.floor(mass / 3) - 2;
}

const input = fs.readFileSync("../input.txt").toString();

let sum = input
  .split("\n")
  .map(line => getFuel(line))
  .reduce((preValue, currentValue) => (preValue += currentValue));

console.log(sum);
