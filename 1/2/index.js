const fs = require("fs");

function getFuel(mass) {
  let fuel = Math.floor(mass / 3) - 2;
  if(fuel <= 0) return 0;

  return fuel + getFuel(fuel);
}

const input = fs.readFileSync("../input.txt").toString();

let sum = input
  .split("\n")
  .map(line => getFuel(line))
  .reduce((preValue, currentValue) => (preValue += currentValue));

console.log(sum)