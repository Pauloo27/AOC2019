const fs = require("fs");

/*
  That one gonna take a long time to run...
  `index.js` fix that.
*/

const directionMapper = {
  U: {
    X: 0,
    Y: +1
  },
  D: {
    X: 0,
    Y: -1
  },
  R: {
    X: +1,
    Y: 0
  },
  L: {
    X: -1,
    Y: 0
  }
};

function getCoords(moves, wire1Coords) {
  let coords = [];
  let x = 0;
  let y = 0;
  moves.forEach(move => {
    let direction = move[0];
    let amount = move.slice(1);
    let directionModifier = directionMapper[direction];

    for (let i = 0; i < amount; i++) {
      x += directionModifier.X;
      y += directionModifier.Y;

      if (wire1Coords === undefined) {
        coords.push(`${x},${y}`);
      } else if (wire1Coords.includes(`${x},${y}`)) {
        coords.push(`${x},${y}`);
      }
    }
  });
  return coords;
}

function getIntesections(wire1, wire2) {
  const wire1Coords = getCoords(wire1.split(","));

  return getCoords(wire2.split(","), wire1Coords);
}

const input = fs.readFileSync("../input.txt").toString();

const intersections = getIntesections(
  input.split("\n")[0],
  input.split("\n")[1]
);

let closest = undefined;

intersections.forEach(intersection => {
  if (closest === undefined) closest = intersection;
  else {
    let [cX, cY] = closest.split(",");
    let [iX, iY] = intersection.split(",");

    let cDistance = Math.abs(cX) + Math.abs(cY);
    let iDistance = Math.abs(iX) + Math.abs(iY);

    if (iDistance < cDistance) {
      closest = intersection;
    }
  }
});

console.log(intersections);
let [cX, cY] = closest.split(",");
console.log(`${closest} ${Math.abs(cX) + Math.abs(cY)}`);
