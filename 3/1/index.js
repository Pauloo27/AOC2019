const fs = require("fs");

class Coord {
  x;
  y;
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  get distanceFromCenter() {
    return Math.abs(this.x) + Math.abs(this.y);
  }

  toString() {
    return `${this.x},${this.y}`;
  }
}

const Orientation = {
  H: "Horizontal",
  V: "Vertical"
};

function isBetween(n, start, end) {
  min = Math.min(start, end);
  max = Math.max(start, end);
  return n > min && n < max;
}

class Line {
  start;
  end;
  constructor(start, end) {
    this.start = start;
    this.end = end;
  }

  toString() {
    return `${this.start} to ${this.end}`;
  }

  get orientation() {
    if (this.start.x === this.end.x) {
      return Orientation.V;
    } else {
      return Orientation.H;
    }
  }

  isParalel(anotherLine) {
    if (this.orientation !== anotherLine.orientation) return false;

    if (this.orientation === Orientation.H) {
      return this.start.y !== anotherLine.start.y;
    }

    if (this.orientation === Orientation.V)
      return this.start.x !== anotherLine.start.x;
  }

  doCross(anotherLine) {
    if (this.isParalel(anotherLine)) return false;

    if (this.orientation === Orientation.H) {
      return (
        isBetween(this.start.y, anotherLine.start.y, anotherLine.end.y) &&
        isBetween(anotherLine.start.x, this.start.x, this.end.x)
      );
    } else {
      return (
        isBetween(this.start.x, anotherLine.start.x, anotherLine.end.x) &&
        isBetween(anotherLine.start.y, this.start.y, this.end.y)
      );
    }
  }

  getIntersection(anotherLine) {
    if (!this.doCross(anotherLine)) return null;
    let x = 0;
    let y = 0;
    if (this.orientation === Orientation.H) {
      x = anotherLine.start.x;
      y = this.start.y;
    } else {
      x = this.start.x;
      y = anotherLine.start.y;
    }
    return new Coord(x, y);
  }
}

const directionMapper = {
  U: new Coord(0, +1),
  D: new Coord(0, -1),
  R: new Coord(+1, 0),
  L: new Coord(-1, 0)
};

function getVertexs(moves) {
  let x = 0;
  let y = 0;
  let vertexs = [];

  moves.forEach(move => {
    let direction = move[0];
    let amount = Number.parseInt(move.slice(1));
    let directionModifier = directionMapper[direction];

    x += amount * directionModifier.x;
    y += amount * directionModifier.y;

    vertexs.push(new Coord(x, y));
  });
  return vertexs;
}

const input = fs.readFileSync("../input.txt").toString();

const wire1 = getVertexs(input.split("\n")[0].split(","));
const wire2 = getVertexs(input.split("\n")[1].split(","));

function getLines(wire) {
  let lines = [];
  for (let i = 0; i < wire.length; i++) {
    let end = wire[i];
    let start = wire[i - 1];
    if (i === 0) {
      if (end.x === 0) {
        start = new Coord(0, 0);
      } else if (end.y === 0) {
        start = new Coord(0, 0);
      }
    }

    lines.push(new Line(start, end));
  }
  return lines;
}

const lines1 = getLines(wire1);
const lines2 = getLines(wire2);

let closest = undefined;

lines1.forEach(line1 => {
  lines2.forEach(line2 => {
    if (line1.doCross(line2)) {
      let intersection = line1.getIntersection(line2);
      if (closest === undefined) {
        closest = intersection;
      } else {
        if (closest.distanceFromCenter > intersection.distanceFromCenter) {
          closest = intersection;
        }
      }
    }
  });
});

console.log(closest.distanceFromCenter);
