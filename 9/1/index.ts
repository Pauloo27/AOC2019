import * as fs from "fs";
import { Computer, parseCode } from "./Computer";

const code = fs.readFileSync("../input.txt").toString();
// const code = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
// const code = "1102,34915192,34915192,7,4,7,99,0";
// const code = "104,1125899906842624,99";

async function main() {
  const comp = new Computer(parseCode(code), [1]);
  const result = await comp.run();
  console.log(result.output);
}

main();
