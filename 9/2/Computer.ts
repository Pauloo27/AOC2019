import { EventEmitter } from "events";

class OperationReturn {
  returnValue?: number;
  output?: number;
  newCodeOffset?: number;
  newRelativeParamOffset?: number;

  constructor(
    returnValue?: number,
    output?: number,
    newCodeOffset?: number,
    newRelativeParamOffset?: number
  ) {
    this.returnValue = returnValue;
    this.output = output;
    this.newCodeOffset = newCodeOffset;
    this.newRelativeParamOffset = newRelativeParamOffset;
  }
}

abstract class Operation {
  parameterCount: number;
  useInput: boolean;

  constructor(parameterCount: number, useInput: boolean) {
    this.parameterCount = parameterCount;
    this.useInput = useInput;
  }

  abstract process(input: number, ...parameters: number[]): OperationReturn;
}

class GenericOperation extends Operation {
  processFunction: Function;
  constructor(
    parameterCount: number,
    useInput: boolean,
    processFunction: Function
  ) {
    super(parameterCount, useInput);
    this.processFunction = processFunction;
  }

  process(input: number, ...parameters: number[]): OperationReturn {
    return this.processFunction(input, ...parameters);
  }
}

class MathOperation extends GenericOperation {
  constructor(mathFunction: Function) {
    super(2, false, (input: number, ...parameters: number[]) => {
      return new OperationReturn(
        mathFunction(parameters[0], parameters[1]),
        undefined
      );
    });
  }
}

const ADDITION = new MathOperation((n1, n2) => n1 + n2);
const MULTIPLICATION = new MathOperation((n1, n2) => n1 * n2);

const INPUT = new GenericOperation(0, true, (input: number) => {
  return new OperationReturn(input, undefined, undefined);
});

const OUTPUT = new GenericOperation(
  1,
  false,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(undefined, parameters[0]);
  }
);

const JUMP_IF_TRUE = new GenericOperation(
  2,
  false,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(
      undefined,
      undefined,
      parameters[0] === 0 ? undefined : parameters[1]
    );
  }
);

const JUMP_IF_FALSE = new GenericOperation(
  2,
  false,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(
      undefined,
      undefined,
      parameters[0] === 0 ? parameters[1] : undefined
    );
  }
);

const LESS_THAN = new GenericOperation(
  2,
  false,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(parameters[0] < parameters[1] ? 1 : 0);
  }
);

const EQUALS = new GenericOperation(
  2,
  false,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(parameters[0] === parameters[1] ? 1 : 0);
  }
);

const MODIFY_RELATIVE_PARAM_OFFSET = new GenericOperation(
  1,
  false,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(undefined, undefined, undefined, parameters[0]);
  }
);

function getOperarionByCode(code: number): Operation {
  switch (code) {
    case 1:
      return ADDITION;
    case 2:
      return MULTIPLICATION;
    case 3:
      return INPUT;
    case 4:
      return OUTPUT;
    case 5:
      return JUMP_IF_TRUE;
    case 6:
      return JUMP_IF_FALSE;
    case 7:
      return LESS_THAN;
    case 8:
      return EQUALS;
    case 9:
      return MODIFY_RELATIVE_PARAM_OFFSET;
  }
}

export class CodeResult {
  code: Array<number>;
  output: Array<number>;

  constructor(code: Array<number>, output: Array<number>) {
    this.code = code;
    this.output = output;
  }
}

enum ParamMode {
  POSITION = 0,
  IMMEDIATE = 1,
  RELATIVE = 2
}

function paramModeFromId(id: number): ParamMode {
  switch (id) {
    case 0:
      return ParamMode.POSITION;
    case 1:
      return ParamMode.IMMEDIATE;
    case 2:
      return ParamMode.RELATIVE;
  }
}

class Instruction {
  opCode: number;
  paramsMode: Array<ParamMode>;

  constructor(opCode: number, paramsMode?: Array<ParamMode>) {
    this.opCode = opCode;
    if (paramsMode) {
      this.paramsMode = paramsMode;
    } else {
      this.paramsMode = new Array<ParamMode>();
    }
  }

  paramModeFor(paramIndex: number): ParamMode {
    if (this.paramsMode.length > paramIndex) {
      return this.paramsMode[paramIndex];
    } else {
      return ParamMode.POSITION;
    }
  }
}

function parseInstruction(instruction: string): Instruction {
  const len = instruction.length;
  if (len === 1) {
    return new Instruction(Number.parseInt(instruction));
  }

  let index = 0;

  let opCode = instruction[len - ++index] + instruction[len - ++index];
  opCode = opCode
    .split("")
    .reverse()
    .join("");

  const paramsMode = new Array<ParamMode>();

  while (index < len) {
    const paramMode = instruction[len - ++index];
    paramsMode.push(paramModeFromId(Number.parseInt(paramMode)));
  }
  return new Instruction(Number.parseInt(opCode), paramsMode);
}

let id = 0;
export class Computer extends EventEmitter {
  codes: Array<number>;
  inputs: Array<number>;
  offset?: number;
  relativeParamOffset?: number;
  output?: Array<number>;

  insertedInputs = new Array<number>();
  paused = false;
  id: number;
  halted = false;

  constructor(
    codes: Array<number>,
    inputs: Array<number>,
    offset?: number,
    relativeParamOffset?: number,
    output?: Array<number>
  ) {
    super();
    this.id = id++;

    this.codes = codes;
    this.inputs = inputs;
    this.offset = offset;
    this.relativeParamOffset = relativeParamOffset;
    this.output = output;

    this.on("input", () => {
      if (this.paused) {
        this.resume();
      }
    });
  }

  input(value: number) {
    this.insertedInputs.push(value);
    this.emit("input");
  }

  async run(): Promise<CodeResult> {
    const promise = new Promise<CodeResult>(resolve => {
      this.on("halt", result => {
        resolve(result);
      });
    });
    this.runCode();
    return promise;
  }

  exit() {
    this.emit("exit");
    this.halt(new CodeResult(this.codes, this.output), "exited");
  }

  private halt(result: CodeResult, reason?: string) {
    this.emit("halt", result);
    this.halted = true;
  }

  private pause() {
    if (this.paused) {
      console.log(
        `Can't pause computer with id ${this.id} because it's already paused`
      );
      process.exit(-5);
    }
    this.emit("pause");
    this.paused = true;
  }

  private resume() {
    this.paused = false;
    this.runCode();
    this.emit("resume");
  }

  private runCode() {
    while (!this.halted) {
      this.runInstruction();
    }
  }

  private runInstruction() {
    if (this.offset === undefined) this.offset = 0;
    if (this.relativeParamOffset === undefined) this.relativeParamOffset = 0;
    if (this.output === undefined) this.output = new Array<number>();

    let index = this.offset;

    const instruction = parseInstruction(this.codes[index].toString());

    if (instruction.opCode === 99)
      return this.halt(new CodeResult(this.codes, this.output));

    const operation = getOperarionByCode(instruction.opCode);
    const params = new Array<number>();

    while (params.length < operation.parameterCount) {
      const paramCode = this.codes[++index];
      const paramMode = instruction.paramModeFor(params.length);
      if (paramMode === ParamMode.IMMEDIATE) {
        params.push(paramCode);
      } else if (paramMode === ParamMode.POSITION) {
        if (this.codes.length > paramCode) {
          params.push(this.codes[paramCode]);
        } else {
          params.push(0);
        }
      } else {
        if (this.codes.length > this.relativeParamOffset + paramCode) {
          params.push(this.codes[this.relativeParamOffset + paramCode]);
        } else {
          params.push(0);
        }
      }
    }

    if (operation.useInput && this.inputs.length === 0) {
      if (this.insertedInputs.length > 0) {
        this.inputs.push(this.insertedInputs[0]);
        this.insertedInputs = this.insertedInputs.slice(1);
      } else {
        this.pause();
        return;
      }
    }
    const input = this.inputs[0];

    const result = operation.process(input, ...params);

    if (operation.useInput) {
      this.inputs = this.inputs.slice(1);
    }

    if (result.returnValue !== undefined) {
      const returnIndex = this.codes[++index];
      if (instruction.paramModeFor(params.length) === ParamMode.RELATIVE) {
        this.codes[this.relativeParamOffset + returnIndex] = result.returnValue;
      } else {
        this.codes[returnIndex] = result.returnValue;
      }
    }

    if (result.output !== undefined) {
      this.output.push(result.output);
      this.emit("output", result.output);
    }

    if (result.newRelativeParamOffset !== undefined) {
      this.relativeParamOffset =
        this.relativeParamOffset + result.newRelativeParamOffset;
    }

    if (result.newCodeOffset === undefined) {
      index++;
    } else {
      index = result.newCodeOffset;
    }

    this.offset = index;
  }
}

export function parseCode(str: string): Array<number> {
  return str.split(",").map(e => Number.parseInt(e));
}
