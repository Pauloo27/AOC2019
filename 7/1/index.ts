import * as fs from "fs";

const inputFile = fs.readFileSync("../input.txt").toString();

class OperationReturn {
  returnValue?: number;
  output?: number;
  newCodeIndex?: number;
  readedInput?: boolean;

  constructor(
    returnValue?: number,
    output?: number,
    newCodeIndex?: number,
    readedInput?: boolean
  ) {
    this.returnValue = returnValue;
    this.output = output;
    this.newCodeIndex = newCodeIndex;
    if (readedInput === undefined) {
      this.readedInput = false;
    } else {
      this.readedInput = readedInput;
    }
  }
}

abstract class Operation {
  parameterCount: number;

  constructor(parameterCount: number) {
    this.parameterCount = parameterCount;
  }

  abstract process(input: number, ...parameters: number[]): OperationReturn;
}

class GenericOperation extends Operation {
  processFunction: Function;
  constructor(parameterCount: number, processFunction: Function) {
    super(parameterCount);
    this.processFunction = processFunction;
  }

  process(input: number, ...parameters: number[]): OperationReturn {
    return this.processFunction(input, ...parameters);
  }
}

class MathOperation extends GenericOperation {
  constructor(mathFunction: Function) {
    super(2, (input: number, ...parameters: number[]) => {
      return new OperationReturn(
        mathFunction(parameters[0], parameters[1]),
        undefined
      );
    });
  }
}

const ADDITION = new MathOperation((n1, n2) => n1 + n2);
const MULTIPLICATION = new MathOperation((n1, n2) => n1 * n2);

const INPUT = new GenericOperation(0, (input: number) => {
  return new OperationReturn(input, undefined, undefined, true);
});

const OUTPUT = new GenericOperation(
  1,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(undefined, parameters[0]);
  }
);

const JUMP_IF_TRUE = new GenericOperation(
  2,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(
      undefined,
      undefined,
      parameters[0] === 0 ? undefined : parameters[1]
    );
  }
);

const JUMP_IF_FALSE = new GenericOperation(
  2,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(
      undefined,
      undefined,
      parameters[0] === 0 ? parameters[1] : undefined
    );
  }
);

const LESS_THAN = new GenericOperation(
  2,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(parameters[0] < parameters[1] ? 1 : 0);
  }
);

const EQUALS = new GenericOperation(
  2,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(parameters[0] === parameters[1] ? 1 : 0);
  }
);

function getOperarionByCode(code: number): Operation {
  switch (code) {
    case 1:
      return ADDITION;
    case 2:
      return MULTIPLICATION;
    case 3:
      return INPUT;
    case 4:
      return OUTPUT;
    case 5:
      return JUMP_IF_TRUE;
    case 6:
      return JUMP_IF_FALSE;
    case 7:
      return LESS_THAN;
    case 8:
      return EQUALS;
  }
}

class CodeResult {
  code: Array<number>;
  output: Array<number>;

  constructor(code: Array<number>, output: Array<number>) {
    this.code = code;
    this.output = output;
  }
}

enum ParamMode {
  POSITION = 0,
  IMMEDIATE = 1
}

function paramModeFromId(id: number): ParamMode {
  switch (id) {
    case 0:
      return ParamMode.POSITION;
    case 1:
      return ParamMode.IMMEDIATE;
  }
}

class Instruction {
  opCode: number;
  paramsMode: Array<ParamMode>;

  constructor(opCode: number, paramsMode?: Array<ParamMode>) {
    this.opCode = opCode;
    if (paramsMode) {
      this.paramsMode = paramsMode;
    } else {
      this.paramsMode = new Array<ParamMode>();
    }
  }

  paramModeFor(paramIndex: number): ParamMode {
    if (this.paramsMode.length > paramIndex) {
      return this.paramsMode[paramIndex];
    } else {
      return ParamMode.POSITION;
    }
  }
}

function parseInstruction(instruction: string): Instruction {
  const len = instruction.length;
  if (len === 1) {
    return new Instruction(Number.parseInt(instruction));
  }

  let index = 0;

  let opCode = instruction[len - ++index] + instruction[len - ++index];
  opCode = opCode
    .split("")
    .reverse()
    .join("");

  const paramsMode = new Array<ParamMode>();

  while (index < len) {
    const paramMode = instruction[len - ++index];
    paramsMode.push(paramModeFromId(Number.parseInt(paramMode)));
  }
  return new Instruction(Number.parseInt(opCode), paramsMode);
}

function runCode(
  codes: Array<number>,
  inputs: Array<number>,
  offset?: number,
  output?: Array<number>
): CodeResult {
  if (offset === undefined) offset = 0;
  if (output === undefined) output = new Array<number>();

  let index = offset;

  const instruction = parseInstruction(codes[index].toString());

  if (instruction.opCode === 99) return new CodeResult(codes, output);

  const operation = getOperarionByCode(instruction.opCode);
  const params = new Array<number>();

  while (params.length < operation.parameterCount) {
    const paramCode = codes[++index];
    const paramMode = instruction.paramModeFor(params.length);
    if (paramMode === ParamMode.IMMEDIATE) {
      params.push(paramCode);
    } else {
      params.push(codes[paramCode]);
    }
  }

  const input = inputs[0];

  const result = operation.process(input, ...params);

  if (result.readedInput !== undefined && result.readedInput) {
    inputs = inputs.slice(1);
  }

  if (result.returnValue !== undefined) {
    const returnIndex = codes[++index];
    codes[returnIndex] = result.returnValue;
  }

  if (result.output !== undefined) {
    output.push(result.output);
  }

  if (result.newCodeIndex === undefined) {
    index++;
  } else {
    index = result.newCodeIndex;
  }

  return runCode(codes, inputs, index, output);
}

function parseCode(str: string): Array<number> {
  return str.split(",").map(e => Number.parseInt(e));
}

function test(
  value: string,
  inputs: Array<number>,
  expectedCode: string,
  expectedOutput: string
) {
  const result = runCode(parseCode(value), inputs);
  const code = result.code.join(",");
  const output = result.output.join(",");
  if (expectedCode !== undefined && code !== expectedCode) {
    console.log(`${value} expected code ${expectedCode} but got ${code}`);
    process.exit(-1);
  }

  if (expectedOutput !== undefined && output !== expectedOutput) {
    console.log(`${value} expected output ${expectedOutput} but got ${output}`);
    process.exit(-1);
  }
}

console.log("Running tests...");
test(
  "1,9,10,3,2,3,11,0,99,30,40,50",
  [0],
  "3500,9,10,70,2,3,11,0,99,30,40,50",
  undefined
);
test("1,0,0,0,99", [0], "2,0,0,0,99", undefined);
test("2,4,4,5,99,0", [0], "2,4,4,5,99,9801", undefined);
test("1,1,1,4,99,5,6,0,99", [0], "30,1,1,4,2,5,6,0,99", undefined);

test("3,0,4,0,99", [23], undefined, "23");

test("3,9,8,9,10,9,4,9,99,-1,8", [8], undefined, "1");
test("3,9,8,9,10,9,4,9,99,-1,8", [1], undefined, "0");
test("3,3,1108,-1,8,3,4,3,99", [8], undefined, "1");
test("3,3,1108,-1,8,3,4,3,99", [1], undefined, "0");
test("3,3,1107,-1,8,3,4,3,99", [8], undefined, "0");
test("3,3,1107,-1,8,3,4,3,99", [15], undefined, "0");
test("3,3,1107,-1,8,3,4,3,99", [-8], undefined, "1");
test("3,3,1107,-1,8,3,4,3,99", [1], undefined, "1");

test("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", [0], undefined, "0");
test("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", [1], undefined, "1");
test("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", [-1], undefined, "1");

test("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", [0], undefined, "0");
test("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", [1], undefined, "1");
test("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", [-1], undefined, "1");

test(
  "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
  [1],
  undefined,
  "999"
);
test(
  "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
  [8],
  undefined,
  "1000"
);
test(
  "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
  [20],
  undefined,
  "1001"
);
console.log("All tests OK");

console.log();
console.log("Running input resolver");

function runWithPhases(code: Array<number>, phases: Array<number>): number {
  let output = 0;
  for (let i = 0; i < 5; i++) {
    // output = runCode(parseCode(inputFile), [phases[i], output]).output[0];
    output = runCode(code, [phases[i], output]).output[0];
  }
  return output;
}

class Strongest {
  phases: Array<number>;
  value: number;
  constructor(phases, value) {
    this.phases = phases;
    this.value = value;
  }

  toString() {
    return `${this.value} with phases ${this.phases}`;
  }
}

function testStrongest(code: string, expectedValue, expectedPhases: string) {
  let strongest: Strongest;

  for (let a = 0; a < 5; a++) {
    for (let b = 0; b < 5; b++) {
      for (let c = 0; c < 5; c++) {
        for (let d = 0; d < 5; d++) {
          for (let e = 0; e < 5; e++) {
            const phases = [...new Set<number>([a, b, c, d, e])];
            if (phases.length !== 5) continue;
            const power = runWithPhases(parseCode(code), phases);
            if (strongest === undefined || power > strongest.value) {
              strongest = new Strongest(phases, power);
            }
          }
        }
      }
    }
  }

  if (
    strongest.value !== expectedValue ||
    strongest.phases.join(",") !== expectedPhases
  ) {
    console.log(
      `Expected ${expectedValue} with ${expectedPhases} but got ${strongest.toString()}`
    );
    process.exit(-1);
  }
}

testStrongest(
  "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0",
  43210,
  "4,3,2,1,0"
);

testStrongest(
  "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0",
  54321,
  "0,1,2,3,4"
);

testStrongest(
  "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0",
  65210,
  "1,0,4,3,2"
);

let strongest: Strongest;

for (let a = 0; a < 5; a++) {
  for (let b = 0; b < 5; b++) {
    for (let c = 0; c < 5; c++) {
      for (let d = 0; d < 5; d++) {
        for (let e = 0; e < 5; e++) {
          const phases = [...new Set<number>([a, b, c, d, e])];
          if (phases.length !== 5) continue;
          const power = runWithPhases(parseCode(inputFile), phases);
          if (strongest === undefined || power > strongest.value) {
            strongest = new Strongest(phases, power);
          }
        }
      }
    }
  }
}

console.log(strongest.toString());
