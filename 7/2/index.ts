import { parseCode, Computer } from "./Computer";
import * as fs from "fs";
import { runTests } from "./tester";

runTests();

class Strongest {
  phases: Array<number>;
  value: number;
  constructor(phases, value) {
    this.phases = phases;
    this.value = value;
  }

  toString() {
    return `${this.value} with phases ${this.phases}`;
  }
}

const inputFile = fs.readFileSync("../input.txt").toString();

function addListener(comp1: Computer, comp2: Computer) {
  comp1.on("output", out => comp2.input(out));
}

async function runWithPhases(
  code: Array<number>,
  phases: Array<number>
): Promise<number> {
  const computers = [
    new Computer(code, [phases[0], 0]),
    new Computer(code, [phases[1]]),
    new Computer(code, [phases[2]]),
    new Computer(code, [phases[3]]),
    new Computer(code, [phases[4]])
  ];

  const [a, b, c, d, e] = computers;

  addListener(a, b);
  const promise = a.run();

  addListener(b, c);
  b.run();

  addListener(c, d);
  c.run();

  addListener(d, e);
  d.run();

  addListener(e, a);

  e.run();

  await promise;
  return e.output[a.output.length - 1];
}

let strongest: Strongest;
async function main() {
  for (let a = 5; a <= 9; a++) {
    for (let b = 5; b <= 9; b++) {
      for (let c = 5; c <= 9; c++) {
        for (let d = 5; d <= 9; d++) {
          for (let e = 5; e <= 9; e++) {
            const phases = [...new Set<number>([a, b, c, d, e])];
            if (phases.length !== 5) continue;
            const power = await runWithPhases(parseCode(inputFile), phases);
            if (strongest === undefined || power > strongest.value) {
              strongest = new Strongest(phases, power);
            }
          }
        }
      }
    }
  }
  console.log(strongest);
}

console.log("Running main");
main();
// const timer = setTimeout(() => console.log("timedout"), 5000);
