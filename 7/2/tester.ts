import { parseCode, Computer } from "./Computer";

async function test(
  value: string,
  inputs: Array<number>,
  expectedCode: string,
  expectedOutput: string
) {
  const result = await new Computer(parseCode(value), inputs).run();
  const code = result.code.join(",");
  const output = result.output.join(",");
  if (expectedCode !== undefined && code !== expectedCode) {
    console.log(`${value} expected code ${expectedCode} but got ${code}`);
    process.exit(-1);
  }

  if (expectedOutput !== undefined && output !== expectedOutput) {
    console.log(`${value} expected output ${expectedOutput} but got ${output}`);
    process.exit(-1);
  }
}

export function runTests() {
  console.log("Running tests...");
  test(
    "1,9,10,3,2,3,11,0,99,30,40,50",
    [0],
    "3500,9,10,70,2,3,11,0,99,30,40,50",
    undefined
  );
  test("1,0,0,0,99", [0], "2,0,0,0,99", undefined);
  test("2,4,4,5,99,0", [0], "2,4,4,5,99,9801", undefined);
  test("1,1,1,4,99,5,6,0,99", [0], "30,1,1,4,2,5,6,0,99", undefined);

  test("3,0,4,0,99", [23], undefined, "23");

  test("3,9,8,9,10,9,4,9,99,-1,8", [8], undefined, "1");
  test("3,9,8,9,10,9,4,9,99,-1,8", [1], undefined, "0");
  test("3,3,1108,-1,8,3,4,3,99", [8], undefined, "1");
  test("3,3,1108,-1,8,3,4,3,99", [1], undefined, "0");
  test("3,3,1107,-1,8,3,4,3,99", [8], undefined, "0");
  test("3,3,1107,-1,8,3,4,3,99", [15], undefined, "0");
  test("3,3,1107,-1,8,3,4,3,99", [-8], undefined, "1");
  test("3,3,1107,-1,8,3,4,3,99", [1], undefined, "1");

  test("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", [0], undefined, "0");
  test("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", [1], undefined, "1");
  test("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", [-1], undefined, "1");

  test("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", [0], undefined, "0");
  test("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", [1], undefined, "1");
  test("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", [-1], undefined, "1");

  test(
    "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
    [1],
    undefined,
    "999"
  );
  test(
    "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
    [8],
    undefined,
    "1000"
  );
  test(
    "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
    [20],
    undefined,
    "1001"
  );
  console.log("All tests OK");
}
