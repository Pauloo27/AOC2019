import * as fs from "fs";

const inputFile = fs.readFileSync("../input.txt").toString();

class OperationReturn {
  returnValue?: number;
  output?: number;

  constructor(returnValue?: number, output?: number) {
    this.returnValue = returnValue;
    this.output = output;
  }
}

abstract class Operation {
  parameterCount: number;

  constructor(parameterCount: number) {
    this.parameterCount = parameterCount;
  }

  abstract process(input: number, ...parameters: number[]): OperationReturn;
}

class GenericOperation extends Operation {
  processFunction: Function;
  constructor(parameterCount: number, processFunction: Function) {
    super(parameterCount);
    this.processFunction = processFunction;
  }

  process(input: number, ...parameters: number[]): OperationReturn {
    return this.processFunction(input, ...parameters);
  }
}

class MathOperation extends GenericOperation {
  constructor(mathFunction: Function) {
    super(2, (input: number, ...parameters: number[]) => {
      return new OperationReturn(
        mathFunction(parameters[0], parameters[1]),
        undefined
      );
    });
  }
}

const ADDITION = new MathOperation((n1, n2) => n1 + n2);
const MULTIPLICATION = new MathOperation((n1, n2) => n1 * n2);

const INPUT = new GenericOperation(0, (input: number) => {
  return new OperationReturn(input, undefined);
});

const OUTPUT = new GenericOperation(
  1,
  (input: number, ...parameters: Array<number>) => {
    return new OperationReturn(undefined, parameters[0]);
  }
);

function getOperarionByCode(code: number): Operation {
  switch (code) {
    case 1:
      return ADDITION;
    case 2:
      return MULTIPLICATION;
    case 3:
      return INPUT;
    case 4:
      return OUTPUT;
  }
}

class CodeResult {
  code: Array<number>;
  output: Array<number>;

  constructor(code: Array<number>, output: Array<number>) {
    this.code = code;
    this.output = output;
  }
}

/*
ABCDE
 1002

DE - two-digit opcode,      02 == opcode 2
 C - mode of 1st parameter,  0 == position mode
 B - mode of 2nd parameter,  1 == immediate mode
 A - mode of 3rd parameter,  0 == position mode,
                                  omitted due to being a leading zero
*/

enum ParamMode {
  POSITION = 0,
  IMMEDIATE = 1
}

function paramModeFromId(id: number): ParamMode {
  switch (id) {
    case 0:
      return ParamMode.POSITION;
    case 1:
      return ParamMode.IMMEDIATE;
  }
}

class Instruction {
  opCode: number;
  paramsMode: Array<ParamMode>;

  constructor(opCode: number, paramsMode?: Array<ParamMode>) {
    this.opCode = opCode;
    if (paramsMode) {
      this.paramsMode = paramsMode;
    } else {
      this.paramsMode = new Array<ParamMode>();
    }
  }

  paramModeFor(paramIndex: number): ParamMode {
    if (this.paramsMode.length > paramIndex) {
      return this.paramsMode[paramIndex];
    } else {
      return ParamMode.POSITION;
    }
  }
}

function parseInstruction(instruction: string): Instruction {
  const len = instruction.length;
  if (len === 1) {
    return new Instruction(Number.parseInt(instruction));
  }

  let index = 0;

  let opCode = instruction[len - ++index] + instruction[len - ++index];
  opCode = opCode
    .split("")
    .reverse()
    .join("");

  const paramsMode = new Array<ParamMode>();

  while (index < len) {
    const paramMode = instruction[len - ++index];
    paramsMode.push(paramModeFromId(Number.parseInt(paramMode)));
  }
  return new Instruction(Number.parseInt(opCode), paramsMode);
}

function runCode(
  codes: Array<number>,
  input: number,
  offset?: number,
  output?: Array<number>
): CodeResult {
  if (offset === undefined) offset = 0;
  if (output === undefined) output = new Array<number>();

  let index = 0 + offset;
  const instruction = parseInstruction(codes[index].toString());

  if (instruction.opCode === 99) return new CodeResult(codes, output);

  const operation = getOperarionByCode(instruction.opCode);
  const params = new Array<number>();

  while (params.length < operation.parameterCount) {
    const paramCode = codes[++index];
    const paramMode = instruction.paramModeFor(params.length);
    if (paramMode === ParamMode.IMMEDIATE) {
      params.push(paramCode);
    } else {
      params.push(codes[paramCode]);
    }
  }

  const result = operation.process(input, ...params);

  if (result.returnValue !== undefined) {
    const returnIndex = codes[++index];
    codes[returnIndex] = result.returnValue;
  }

  if (result.output !== undefined) {
    output.push(result.output);
  }

  return runCode(codes, input, ++index, output);
}

function parseCode(str: string): Array<number> {
  return str.split(",").map(e => Number.parseInt(e));
}

function test(
  value: string,
  input: number,
  expectedCode: string,
  expectedOutput: string
) {
  const result = runCode(parseCode(value), input);
  const code = result.code.join(",");
  const output = result.output.join(",");
  if (expectedCode !== undefined && code !== expectedCode) {
    console.log(`${value} expected code ${expectedCode} but got ${code}`);
    process.exit(-1);
  }

  if (expectedOutput !== undefined && output !== expectedOutput) {
    console.log(`${value} expected output ${expectedOutput} but got ${output}`);
    process.exit(-1);
  }
}

console.log("Running tests...");
test(
  "1,9,10,3,2,3,11,0,99,30,40,50",
  0,
  "3500,9,10,70,2,3,11,0,99,30,40,50",
  undefined
);
test("1,0,0,0,99", 0, "2,0,0,0,99", undefined);
test("2,4,4,5,99,0", 0, "2,4,4,5,99,9801", undefined);
test("1,1,1,4,99,5,6,0,99", 0, "30,1,1,4,2,5,6,0,99", undefined);

test("3,0,4,0,99", 23, undefined, "23");
console.log("All tests OK");

console.log();
console.log("Running input resolver");

const shipId = 1;
const result = runCode(parseCode(inputFile), shipId);
console.log(result.output[result.output.length - 1]);
