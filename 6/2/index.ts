import * as fs from "fs";

const input = fs.readFileSync("../input.txt").toString();
console.log(".")
// const input = `COM)B
// B)C
// C)D
// D)E
// E)F
// B)G
// G)H
// D)I
// E)J
// J)K
// K)L
// K)YOU
// I)SAN`;

class Orbit {
  center: string;
  object: string;

  constructor(center: string, object: string) {
    this.center = center;
    this.object = object;
  }

  toString(): string {
    return `${this.center})${this.object}`;
  }
}

function parseOrbit(str: string): Orbit {
  const splitted = str.split(")");
  return new Orbit(splitted[0], splitted[1]);
}

const orbits = input.split("\n").map(notation => parseOrbit(notation));

function findWhereIsOrbiting(orbits: Array<Orbit>, object: string): Orbit {
  return orbits.find(orbit => orbit.object === object);
}

function findPathTo(object: string, target: string): Array<Orbit> {
  let orbitingOn: Orbit | undefined;
  let orbitingOnList = new Array<Orbit>();
  do {
    if (orbitingOn === undefined) {
      orbitingOn = findWhereIsOrbiting(orbits, object);
    } else {
      orbitingOn = findWhereIsOrbiting(orbits, orbitingOn.center);
    }
    orbitingOnList.push(orbitingOn);
  } while (orbitingOn.center !== target);
  return orbitingOnList;
}

const YOU_TO_COM = findPathTo("YOU", "COM");
const SAN_TO_COM = findPathTo("SAN", "COM");

const commonRoot = SAN_TO_COM.find(orbit => YOU_TO_COM.includes(orbit));

console.log(
  findPathTo(findWhereIsOrbiting(orbits, "YOU").center, commonRoot.object)
    .length +
    findPathTo(findWhereIsOrbiting(orbits, "SAN").center, commonRoot.object)
      .length
);
