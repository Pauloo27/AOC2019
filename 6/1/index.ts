import * as fs from "fs";

const input = fs.readFileSync("../input.txt").toString();

class Orbit {
  center: string;
  object: string;

  constructor(center: string, object: string) {
    this.center = center;
    this.object = object;
  }

  toString(): string {
    return `${this.center})${this.object}`;
  }
}

function parseOrbit(str: string): Orbit {
  const splitted = str.split(")");
  return new Orbit(splitted[0], splitted[1]);
}

const orbits = input.split("\n").map(notation => parseOrbit(notation));

function findOrbtingOn(orbits: Array<Orbit>, center: string): Array<Orbit> {
  return orbits.filter(orbit => orbit.center === center);
}

var indirect = 0;
function countDirectOrbits(center: string, stackLevel?: number): number {
  if (stackLevel === undefined) stackLevel = 0;
  if (stackLevel > 0) indirect += stackLevel - 1;

  const result = findOrbtingOn(orbits, center);
  if (result.length === 0) return 0;

  stackLevel++;

  return (
    result.length +
    result
      .map(orbit => countDirectOrbits(orbit.object, stackLevel))
      .reduce((sum, elem) => (sum += elem))
  );
}
const direct = countDirectOrbits("COM");
console.log(`${direct} + ${indirect} = ${direct + indirect}`);
