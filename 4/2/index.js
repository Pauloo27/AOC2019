const fs = require("fs");

const input = fs.readFileSync("../input.txt").toString();
const min = Number.parseInt(input.split("-")[0]);
const max = Number.parseInt(input.split("-")[1]);

let posibilities = 0;

/*
 * It is a six-digit number.
 * The value is within the range given in your puzzle input.
 * Two adjacent digits are the same (like 22 in 122345).
 * Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
 * The two adjacent matching digits are not part of a larger group of matching digits.
 */
function isValid(str) {
  let double = false;
  let last = -1;
  for (let i = 0; i < 6; i++) {
    let prev = Number.parseInt(str.charAt(i - 1));
    let current = Number.parseInt(str.charAt(i));
    let next = Number.parseInt(str.charAt(i + 1));

    if (!Number.isNaN(prev)) {
      if (prev > current) return false;
      if (prev === current && current !== last && !double) {
        double = true;
        last = current;
      }
    }

    if (!Number.isNaN(next)) {
      if (next < current) return false;
      if (next === current && current !== last && !double) {
        double = true;
        last = current;
      }
    }

    if (current === prev && current === next && current === last) {
      double = false;
    }
  }
  return double;
}

for (let attempt = min; attempt <= max; attempt++) {
  if (isValid(attempt.toString())) posibilities++;
}

console.log(posibilities);
