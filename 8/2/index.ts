import * as fs from "fs";

const input = fs.readFileSync("../input.txt").toString();

function decode(image: string, width: number, height: number): Array<string> {
  const totalPixelCount = image.length;
  const pixelPerLayer = width * height;
  const layersCount = totalPixelCount / pixelPerLayer;

  const layers = new Array<string>();
  for (let i = 0; i < layersCount; i++) {
    layers.push(
      image.substring(i * pixelPerLayer, i * pixelPerLayer + pixelPerLayer)
    );
  }

  const finalImage = Array<string>(height);
  finalImage.fill("2".repeat(width), 0);

  layers.forEach(layer => {
    for (let i = 0; i < layer.length; i++) {
      const pixelValue = layer[i];
      if (pixelValue !== "2") {
        const rowId = Math.floor(i / width);
        const columnId = i - rowId * width;
        if (finalImage[rowId][columnId] === "2") {
          const row = finalImage[rowId];
          let newRow = row.substring(0, columnId);
          newRow = newRow + pixelValue;
          newRow = newRow + row.substring(columnId + 1);
          finalImage[rowId] = newRow;
        }
      }
    }
  });
  return finalImage;
}

const image = decode(input, 25, 6);

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, "g"), replace);
}

image.forEach(row =>
  console.log(replaceAll(replaceAll(row, "1", "█"), "0", " "))
);
