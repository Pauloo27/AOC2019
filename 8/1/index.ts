import * as fs from "fs";

const input = fs.readFileSync("../input.txt").toString();

function count(text: string, element: string): number {
  return text.split("").filter(s => s === element).length;
}

function calc(image: string, width: number, height: number): number {
  const totalPixelCount = image.length;
  const pixelPerLayer = width * height;
  const layersCount = totalPixelCount / pixelPerLayer;

  const layers = new Array<string>();
  for (let i = 0; i < layersCount; i++) {
    layers.push(
      image.substring(i * pixelPerLayer, i * pixelPerLayer + pixelPerLayer)
    );
  }

  const zeroCount = layers.map(layer => count(layer, "0"));

  let lowest: number;
  zeroCount.forEach(count => {
    if (lowest === undefined || count < lowest) {
      lowest = count;
    }
  });

  const layerWithFewerZeros = layers[zeroCount.indexOf(lowest)];

  return count(layerWithFewerZeros, "1") * count(layerWithFewerZeros, "2");
}

console.log(calc(input, 25, 6));
